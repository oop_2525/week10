package com.rangsiman.week10;

public class Rectangle extends Shape{
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        super("Rectangle");
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return this.getName() + " Width:" + this.width + " Height:" + this.height;
    }

    @Override
    public double calArea() {
        return this.width * this.height;
    }

    @Override
    public double calPerimeter() {
        return this.width*2 + this.height*2;
    }
}
