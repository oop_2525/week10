package com.rangsiman.week10;

public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 3);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle = new Circle(2);
        System.out.println(circle.toString());
        System.out.printf("%s Area: %.2f \n",circle.getName(), circle.calArea());
        System.out.printf("%s Perimeter: %.2f \n",circle.getName(), circle.calPerimeter());

        Circle circle2 = new Circle(4);
        System.out.println(circle2.toString());
        System.out.printf("%s Area: %.2f \n",circle2.getName(), circle2.calArea());
        System.out.printf("%s Perimeter: %.2f \n",circle2.getName(), circle2.calPerimeter());

        Triangle triangle = new Triangle(2, 3, 4);
        System.out.println(triangle.toString());
        System.out.printf("%s Area: %.2f \n",triangle.getName(), triangle.calArea());
        System.out.printf("%s Perimeter: %.2f \n",triangle.getName(), triangle.calPerimeter());

        Triangle triangle2 = new Triangle(4, 5, 6);
        System.out.println(triangle2.toString());
        System.out.printf("%s Area: %.2f \n",triangle2.getName(), triangle2.calArea());
        System.out.printf("%s Perimeter: %.2f \n",triangle2.getName(), triangle2.calPerimeter());
    }
}
